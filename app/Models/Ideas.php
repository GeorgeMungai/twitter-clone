<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ideas extends Model
{
    use HasFactory;
    //acts to reduce repeated queries we define the relationship
    //we can sprcify further to columns needed
    protected $with = ['user:id,name,image','comments.user:id,name,image'];

    // protected $guarded =[];

    protected $fillable = [
        'content',
        'like',
        'user_id',

    ];

    public function comments()
    {
        return $this->hasMany(Comment::class, 'idea_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
