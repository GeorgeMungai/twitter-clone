<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class FollowerController extends Controller
{
    public function follow(User $user){
        //creating a many-many relationship between users
        //we use the followings function relationship created ata user model
        //to attach the logged in user {auth()->user} to the model we want
        //to create the relationship for
        $follower = auth()->user();

        $follower ->followings()->attach($user);
        return redirect()->route('users.show', $user->id)->with('success', 'user followed');

    }


    public function unfollow(User $user) {
    $follower = auth()->user();

    // Check if the user is following the specified user
    if ($follower->followings()->where('user_id', $user->id)->exists()) {
        // If the user is following, then unfollow
        $follower->followings()->detach($user);
        return redirect()->route('users.show', $user->id)->with('success', 'User unfollowed');

    }
}
}
