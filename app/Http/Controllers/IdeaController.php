<?php

namespace App\Http\Controllers;

use App\Models\Ideas;
use Illuminate\Http\Request;

class IdeaController extends Controller
{

    public function show(Ideas $id){



        return view('ideas.show',[
            'idea' => $id,
        ]);
    }

    public function store(){
    $validated = request()->validate([
        'content' => 'required|min:3|max:100',
    ]);

    $validated['user_id'] = auth()->id();

    Ideas::create($validated);

    return redirect()->route('dashboard')->with('success', 'Idea was created');
    }
    public function destroy(Ideas $id){
        // if(auth()->id() !== $id->user_id){
        // abort(404);
        // }
        $this->authorize('idea.delete',$id);
        $id->delete();
        return redirect()->route('dashboard')->with('success', 'Idea deleted ');
    }

    public function edit(Ideas $id){
        // if(auth()->id() !== $id->user_id){
        //     abort(404);
        //     }
        $this->authorize('idea.edit', $id);
        $editing = true;

        return view('ideas.show',[
            'idea' => $id,
            'editing' => $editing,
        ]);
    }

    public function update(Ideas $id){

        $this->authorize('idea.update', $id);

        // if(auth()->id() !== $id->user_id){
        //     abort(404);
        //     }
    $validated = request()->validate([
        'content' => 'required|min:3|max:100',
    ]);

    $id->update($validated);

    return redirect()->route('ideas.show', $id->id)->with('success', 'Idea was updated');
    }
}
