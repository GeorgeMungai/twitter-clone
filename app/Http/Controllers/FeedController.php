<?php

namespace App\Http\Controllers;

use App\Models\Ideas;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    public function index(Request $request)
    {

        $user = auth()->user();
        $FollowingIDs = $user->followings()->pluck("user_id");


        $ideas = Ideas::WhereIn('user_id',$FollowingIDs)->latest();

        if ($request->has('search')) {
            $search = '%' . $request->get('search') . '%'; // Preparing the search term
            $ideas = $ideas->where('content', 'like', $search);
        }

        return view('dashboard', [
            'ideas' => $ideas->paginate(5),
        ]);
    }
}

