<?php

namespace App\Http\Controllers;

use App\Models\Ideas;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
     // check if there is a search
    // if there is check the search value with our database
{
    //eager loading we reduce the number of quereies being passed
    //make efficiency define ideas with user function the
    //define the relationship in the model
    //also can use proctected function
    $ideas = Ideas::orderBy('created_at', 'DESC');

    if (request()->has('search')) {
        $search= '%' . request()->get('search') . '%'; // Preparing the search term
        $ideas = $ideas->where('content', 'like', $search);
    }

    return view('dashboard', [
        'ideas' => $ideas->paginate(5),
    ]);
}
}
