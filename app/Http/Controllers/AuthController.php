<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeEmail;
use App\Models\User;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function register(){
        return view ('auth.register');
    }

    public function store(){


        $validated = request()->validate([
            'name' => 'required|min:3|max:30',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
        ]);

        $user = User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => Hash::make($validated['password'],)


        ]);

        //in order to send mail to a created user we add the variabl user to the create function
        //and then using mail function indicate to the new mail function created linked by $user
        //then define our server configurations
        Mail::to($user->email)->send(new WelcomeEmail($user));

        return redirect()->route('dashboard')->with('success', 'User registered');

        // Validate
        // create user

        // register
    }
    public function login(){
        return view ('auth.login');
    }

    public function authenticate(){


        $validated = request()->validate([

            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (auth()->attempt($validated)){

            request()->session()->regenerate();
            return redirect()->route('dashboard')->with('success', 'user logged in');

        }



        return redirect()->route('log in')->withErrors([
            'email' => 'wrong email',
            'password' => 'wrong password',
        ]);

        // Validate
        // create user

        // register
    }
    public function logout()
    {
    auth()->logout();
    request()->session()->invalidate();
    request()->session()->regenerateToken();

    return redirect()->route('dashboard')->with('success', 'User logged out');
    }


}
