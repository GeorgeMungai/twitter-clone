<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Ideas;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request, $id)
{
    $request->validate([
        'content' => 'required|min:3|max:255',
    ]);

    // Create a new comment instance
    $comment = new Comment();

    // Set the idea_id and user_id for the comment
    $comment->idea_id = $id; // Assuming $id is the idea_id passed from the route
    $comment->user_id = auth()->id();
    $comment->content = $request->input('content');

    // Save the comment
    $comment->save();

    // Redirect back to the idea show page
    return redirect()->route('ideas.show', $id)->with('success', 'Comment added.');
}
}
