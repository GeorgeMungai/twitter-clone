<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FeedController;
use App\Http\Controllers\FollowerController;
use App\Http\Controllers\IdeaController;
use App\Http\Controllers\Admin\DashboardController as AdminDashBoardController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[DashboardController::class,'index'])->name('dashboard');
Route::get('/terms',function(){return view('feed');})->name('terms');
Route::resource('users', UserController::class)->only('edit','show','update')->middleware('auth');
Route::get('profile',[UserController::class,'profile'])->name('profile')->middleware('auth');

Route::group(['prefix'=> 'ideas/', 'as'=>'ideas.'], function() {

    Route::post('',[IdeaController::class,'store'])->name('store');
    Route::get('{id}',[IdeaController::class,'show'])->name('show');

    Route::get('{id}/edit',[IdeaController::class,'edit'])->name('edit')->middleware('auth');
    Route::put('{id}',[IdeaController::class,'update'])->name('update')->middleware('auth');

    Route::delete('{id}',[IdeaController::class,'destroy'])->name('destroy')->middleware('auth');





});

// Route::resource('ideas', IdeaController::class)->except(['index','create'])->middleware('auth');

Route::resource('ideas.comments', CommentController::class)->only(['store'])->middleware('auth');


Route::post('users/{user}/follow', [FollowerController::class, 'follow'])->middleware('auth')->name('users.follow');


Route::post('users/{user}/unfollow', [FollowerController::class, 'unfollow'])->middleware('auth')->name('users.unfollow');
// Model

// Model
// controller
// Migration
// routes

Route::get('/feed',[FeedController::class, 'index'])->middleware('auth')->name('feed');

Route::get('/admin',[AdminDashBoardController::class, 'index'])->middleware(['auth','can:admin'])->name('admin.dashboard');
