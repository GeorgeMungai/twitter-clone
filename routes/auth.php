
<?php

use App\Http\Controllers\AuthController;
use Illuminate\support\Facades\Route;


Route::post('/register', [AuthController::class, 'store'])->name('auth.store');

Route::get('/register', [AuthController::class, 'register'])->name('register');

Route::post('/login', [AuthController::class, 'authenticate'])->name('login.store');

Route::get('/login', [AuthController::class, 'login'])->name('login');

Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
